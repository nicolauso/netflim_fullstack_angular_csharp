import { Component, inject, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BannerNetflimComponent } from '../banner-netflim/banner-netflim.component';
import { UserService } from '../../core/services/userService';
import { User } from '../../core/models/user';
import {
  FormGroup,
  FormControl,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { isAction } from '@reduxjs/toolkit';
@Component({
  selector: 'app-admin',
  standalone: true,
  imports: [BannerNetflimComponent, CommonModule, ReactiveFormsModule],
  template: `
    <app-banner-netflim class="bannerNeftlim"></app-banner-netflim>

    <h1 class="adminTitle">Admin - Manage accounts</h1>

    <div>
      <div class="adminContainer" id="headerAdminContainer">
        <div>Id</div>
        <div>Firstname</div>
        <div>Lastname</div>
        <div>Email</div>
        <div></div>
        <div></div>
        <div></div>
      </div>

      <form
        *ngFor="let user of userList"
        class="adminContainer"
        id="eachUserContainer"
        [formGroup]="editUserForm"
      >
        <div>{{ user.userId }}</div>
        <div [hidden]="user.isActive">{{ user.firstNameUser }}</div>
        <input
          type="text"
          formControlName="FirstNameUser"
          [placeholder]="user.firstNameUser"
          [hidden]="!user.isActive"
        />
        <div [hidden]="user.isActive">{{ user.lastNameUser }}</div>
        <input
          type="text"
          formControlName="LastNameUser"
          [placeholder]="user.lastNameUser"
          [hidden]="!user.isActive"
        />
        <div [hidden]="user.isActive">{{ user.emailUser }}</div>
        <input
          type="text"
          formControlName="EmailUser"
          [placeholder]="user.emailUser"
          [hidden]="!user.isActive"
        />
        <div (click)="editingUser(user)" (click)="toggleFormControl(user)">
          <span class="material-symbols-rounded"> edit </span>
        </div>
        <div><span class="material-symbols-rounded"> delete </span></div>
        <div (click)="saveUser(user)" (click)="toggleFormControl(user)">
          <span class="material-symbols-rounded"> save </span>
        </div>
      </form>
    </div>
  `,

  styleUrl: './admin.component.css',
})
export class AdminComponent {
  @Input() user!: User;
  userService: UserService = inject(UserService);
  isActive = false;
  userList: User[] = [];

  editUserForm!: FormGroup;

  toggleFormControl(user: User) {
    user.isActive = !user.isActive;
  }

  editingUser(user: User) {
    this.editUserForm.patchValue({
      FirstNameUser: user.firstNameUser,
      LastNameUser: user.lastNameUser,
      EmailUser: user.emailUser,
    });
  };



  saveUser(user: User) {
    const editUserFormValues = this.editUserForm.value;
    console.log('AdminComponent editUserFormValues : ', editUserFormValues);

    this.userService.editUser(user.userId, editUserFormValues);

    //Ajouter setTimeout()

    setTimeout(() => {
      this.userService.readUser().then((userList: User[]) => {
      this.userList = userList;
      console.log('userList', userList);
    })
    }, 1000);


  }

  ngOnInit(): void {

    this.editUserForm = new FormGroup({
      FirstNameUser: new FormControl('', Validators.required),
      LastNameUser: new FormControl('', Validators.required),
      EmailUser: new FormControl('', Validators.required),
    });
  }

  constructor() {
    this.userService.readUser().then((userList: User[]) => {
      this.userList = userList;
      console.log('userList', userList);
    });

    // Rappeler readUser() ou modifier UserList(i) après update
  }
}
