import { Component } from '@angular/core';
import { UserFormComponent } from '../../component/add-user-form/add-user-form.component';
import { BannerNetflimComponent } from '../banner-netflim/banner-netflim.component';

@Component({
  selector: 'app-creating-account',
  standalone: true,
  imports: [UserFormComponent, BannerNetflimComponent],
  template: `
  <app-banner-netflim></app-banner-netflim>
    <div class="createContainer">
      <h1>Create an account</h1>
      <app-add-user-form></app-add-user-form>
    </div>

    `,
  styleUrl: './creating-account.component.css',
})
export class CreatingAccountComponent {}
