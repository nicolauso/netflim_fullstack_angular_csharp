import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatingAccountComponent } from './creating-account.component';

describe('CreatingAccountComponent', () => {
  let component: CreatingAccountComponent;
  let fixture: ComponentFixture<CreatingAccountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CreatingAccountComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CreatingAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
