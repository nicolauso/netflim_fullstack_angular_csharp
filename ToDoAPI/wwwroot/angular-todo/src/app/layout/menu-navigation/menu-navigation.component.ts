import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { RouterLink, RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-menu-navigation',
  standalone: true,
  imports: [CommonModule, RouterLink, RouterOutlet],
  template: `
    <nav>
      <a [routerLink]="['/home']">
        <span class="material-symbols-rounded icon"> home </span>
      </a>
      <a [routerLink]="['/popular']">
        <span class="material-symbols-rounded icon"> star </span></a
      >
      <a [routerLink]="['/admin']">
        <span class="material-symbols-rounded icon"> shield_person </span></a
      >
      <a [routerLink]="['/create-account']"
        ><span class="material-symbols-rounded icon"> person_add </span>
      </a>
      <a [routerLink]="['/user-account']"
        ><span class="material-symbols-rounded icon"> person </span>
      </a>
    </nav>
  `,
  styleUrl: './menu-navigation.component.css',
})
export class MenuNavigationComponent {}
