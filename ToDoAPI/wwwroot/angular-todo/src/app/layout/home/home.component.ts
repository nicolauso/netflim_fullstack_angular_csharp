import { LoginService } from './../../core/services/loginService';
import { MovieComponent } from '../../component/movie/movie.component';
import { CommonModule } from '@angular/common';
import { Component, inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BannerNetflimComponent } from '../banner-netflim/banner-netflim.component';
import { SlidesMoviesByGenreComponent } from '../slides-movies-by-genre/slides-movies-by-genre.component';
@Component({
    selector: 'app-home',
    standalone: true,
    template: `
    <app-banner-netflim></app-banner-netflim>
    <h1>Welcome to the movies plateform</h1>
    <button (click)="logOut()">Logout</button>

 <h2>Action</h2>
<app-slides-movies-by-genre [genre]=28></app-slides-movies-by-genre>
<h2>Horror</h2>
<app-slides-movies-by-genre [genre]=27></app-slides-movies-by-genre>
<h2>Animation</h2>
<app-slides-movies-by-genre [genre]=16></app-slides-movies-by-genre>
  `,
    styleUrl: './home.component.css',
    imports: [CommonModule, MovieComponent, FormsModule, BannerNetflimComponent, SlidesMoviesByGenreComponent]
})
export class HomeComponent {
  constructor() {}
  loginService: LoginService = inject(LoginService);
  logOut() {
    this.loginService.logout();
  }
}
