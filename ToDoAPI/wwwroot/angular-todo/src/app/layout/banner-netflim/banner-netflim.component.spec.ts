import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BannerNetflimComponent } from './banner-netflim.component';

describe('BannerNetflimComponent', () => {
  let component: BannerNetflimComponent;
  let fixture: ComponentFixture<BannerNetflimComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BannerNetflimComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(BannerNetflimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
