import { Component } from '@angular/core';

@Component({
  selector: 'app-banner-netflim',
  standalone: true,
  imports: [],
  template : `
  <div class="netflimBannerContainer">
    <h1 class="netflimTitle">Netflim</h1>
     <img src="https://miro.medium.com/v2/resize:fit:720/format:webp/1*5lyavS59mazOFnb55Z6znQ.png"  >
  </div>
  `,
  styleUrl: './banner-netflim.component.css'
})
export class BannerNetflimComponent {

}
