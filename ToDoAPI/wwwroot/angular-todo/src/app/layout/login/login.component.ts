import { Component } from '@angular/core';
import { LoginFormComponent } from '../../component/login-form/login-form.component';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [LoginFormComponent],
  template: `
    <div class="containerLogin">
      <h1 class="netflimTitle">Netflim</h1>
      <div class="loginForm">
        <app-login-form ></app-login-form>
      </div>
    </div>`,
  styleUrl: './login.component.css',
})
export class LoginComponent {

}
