import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { RouterLink, RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-navbar',
  standalone: true,
  imports: [CommonModule, RouterLink, RouterOutlet],
  template: `
   <nav>
      <a [routerLink]="['/home']">Home</a>
      <a [routerLink]="['/popular']">Popular movies</a
      >
      <a [routerLink]="['/admin']">Admin session</a
      >
      <a [routerLink]="['/create-account']">Create account</a>
      <a [routerLink]="['/user-account']">My account</a>
    </nav>
  `,
  styleUrl: './navbar.component.css',
})
export class NavbarComponent {}
