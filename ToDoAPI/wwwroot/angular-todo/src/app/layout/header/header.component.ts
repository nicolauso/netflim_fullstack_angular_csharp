import { MovieService } from '../../core/services/movieService';
import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterLink, RouterOutlet } from '@angular/router';
import { Movie } from '../../core/models/movie';
import { GenresMovies } from '../../core/models/movie';
import { FormsModule } from '@angular/forms';
import { MovieComponent } from '../../component/movie/movie.component';

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [
    CommonModule,
    RouterLink,
    RouterOutlet,
    FormsModule,
    MovieComponent
  ],
  template: `

    <nav>
      <a [routerLink]="['/home']">Home</a>

      <label>
        Movies by genres
        <select
          name="genres"
          id="genre-select"
          class="genre-select"
          [(ngModel)]="genre"
          (change)="onGenreSelect()"
        >
          <option  [ngValue]="null" selected disabled >Movies by genre</option>
          <option *ngFor="let genre of genresMoviesList" [value]="genre.id">
            {{ genre.name }}
          </option>
        </select>
      </label>

      <a [routerLink]="['/popular']">Popular movies</a>
      <a [routerLink]="['/admin']">
         Admin console
      </a>
      <a [routerLink]="['/create-account']">
         Add account
      </a>
      <a [routerLink]="['/user-account']">
        My account
      </a>
    </nav>
<router-outlet></router-outlet>
    <section class="results-movies">
      <app-movie *ngFor="let movie of movieList" [movie]="movie"></app-movie>
    </section>
  `,
  styleUrl: './header.component.css',
})
export class HeaderComponent {
  movieList: Movie[] = [];
  movie!: Movie;
  genresMoviesList: GenresMovies[] = [];
  genre!: number;

  movieService: MovieService = inject(MovieService);

  onGenreSelect() {
    // On select -> diriger vers le composant moviebyGenre avec le genreID)

    //console.log("genresMoviesList", this.genresMoviesList);
    console.log('genre', this.genre);

    if (this.genre) {
      this.movieService
        .getAllMoviesByGenre(this.genre) // Envoie l'ID du genre sélectionné à la fonction
        .then((movieList: Movie[]) => {
          this.movieList = movieList;
          console.log(movieList);
        });
    }
  }
  constructor() {
    this.movieService
      .getAllGenres()
      .then((genresMoviesList: GenresMovies[]) => {
        this.genresMoviesList = genresMoviesList;
      });
  }
}
