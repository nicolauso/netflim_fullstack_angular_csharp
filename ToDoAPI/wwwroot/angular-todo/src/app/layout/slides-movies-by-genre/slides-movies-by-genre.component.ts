import { Movie } from '../../core/models/movie';
import { Component, inject, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovieComponent } from '../../component/movie/movie.component';
import { MovieService } from '../../core/services/movieService';
import { GenresMovies } from '../../core/models/movie';

@Component({
  selector: 'app-slides-movies-by-genre',
  standalone: true,
  imports: [CommonModule, MovieComponent],
  template: `
    <section class="results-movies">
      <app-movie *ngFor="let movie of movieList" [movie]="movie"></app-movie>
    </section>
  `,
  styleUrl: './slides-movies-by-genre.component.css',
})
export class SlidesMoviesByGenreComponent {
  movieList: Movie[] = [];
  movie!: Movie;
  genresMoviesList: GenresMovies[] = [];
  // genre!: number;

  movieService: MovieService = inject(MovieService);

  @Input() genre: number = 0;

  ngOnInit() {
    console.log('this.genre', this.genre);

    this.movieService
      .getAllMoviesByGenre(this.genre) // Envoie l'ID du genre sélectionné à la fonction
      .then((movieList: Movie[]) => {
        this.movieList = movieList;
        console.log(movieList);
      });
  }

  constructor() {}
}

// Mettre un Output()
//
