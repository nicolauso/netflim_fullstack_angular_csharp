import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SlidesMoviesByGenreComponent } from './slides-movies-by-genre.component';

describe('SlidesMoviesByGenreComponent', () => {
  let component: SlidesMoviesByGenreComponent;
  let fixture: ComponentFixture<SlidesMoviesByGenreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SlidesMoviesByGenreComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SlidesMoviesByGenreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
