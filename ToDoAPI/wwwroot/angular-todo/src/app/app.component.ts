import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './layout/header/header.component';
import { MenuNavigationComponent } from './layout/menu-navigation/menu-navigation.component';
import { NavbarComponent } from './layout/navbar/navbar.component';
@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterModule, HeaderComponent, MenuNavigationComponent, NavbarComponent],
  template: `
    <main>
      <!-- <app-header class="header"></app-header> -->
      <section class="content">
        <router-outlet></router-outlet>
      </section>
      <app-menu-navigation class="menu"></app-menu-navigation>
    </main>
  `,
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'angular-todo';
}
