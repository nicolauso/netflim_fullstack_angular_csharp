import { Component, Input, EventEmitter, Output, inject } from '@angular/core';
import { Movie } from '../../core/models/movie';
import { CommonModule } from '@angular/common';
import { RouterLink, RouterOutlet } from '@angular/router';
import { MovieService } from '../../core/services/movieService';
@Component({
  selector: 'app-movie',
  standalone: true,
  imports: [CommonModule, RouterLink, RouterOutlet],
  template: `
    <section class="listing">
      <img
        class="listing-heading"
        src="https://image.tmdb.org/t/p/w342/{{ movie.poster_path }}"
        alt=""
      />

    </section>
  `,
  styleUrl: './movie.component.css',
})
export class MovieComponent {
  @Input() movie!: Movie;
  movieService: MovieService = inject(MovieService);


}
