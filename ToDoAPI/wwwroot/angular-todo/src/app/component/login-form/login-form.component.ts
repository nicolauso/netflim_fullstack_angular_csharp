import { LoginService } from './../../core/services/loginService';
import { Component, inject } from '@angular/core';
import {
  FormGroup,
  FormControl,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-login-form',
  standalone: true,
  imports: [ReactiveFormsModule, RouterLink],
  template: `

    <form [formGroup]="loginForm" (ngSubmit)="onSubmit()" class="loginFormContainer">
    <div class="loginFormContent">
      <h1>Sign in</h1>
      <label for="email"> </label>
        <input
          type="text"
          formControlName="EmailUser"
          placeholder="Enter your email"
        />
        <label for="password"> </label>
        <input
          type="text"
          formControlName="PasswordUser"
          placeholder="Enter your password"
        />
        <button type="submit" class="buttonSubmit">Login</button>
        <button class="createAccountRedirection">
          <a [routerLink]="['/create-account']">
          New to Netflim ?
          <h3>Create an account</h3>
        </a>
        </button>
    </div>

    </form>


  `,
  styleUrl: './login-form.component.css',
})
export class LoginFormComponent {
  loginForm!: FormGroup;
  loginService: LoginService = inject(LoginService);

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      EmailUser: new FormControl('', Validators.required),
      PasswordUser: new FormControl('', Validators.required),
    });
  }

  onSubmit() {
    const loginFormValues = this.loginForm.value;
    this.loginService.login(loginFormValues);
    this.loginForm.reset();
  }
}
