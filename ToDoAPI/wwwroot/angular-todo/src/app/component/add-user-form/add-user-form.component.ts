import { Component, inject } from '@angular/core';
import {
  FormGroup,
  FormControl,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { UserService } from '../../core/services/userService';

@Component({
  selector: 'app-add-user-form',
  standalone: true,
  template: `
    <form [formGroup]="addUserForm" (ngSubmit)="onSubmit()" class="addUserForm">
      <label for="first-name">First Name: </label>
      <input
        type="text"
        formControlName="FirstNameUser"
        placeholder="Enter your firstname"
      />

      <label for="last-name">Last Name: </label>
      <input
        type="text"
        formControlName="LastNameUser"
        placeholder="Enter your lastname"
      />
      <label for="email">Email: </label>
      <input
        type="text"
        formControlName="EmailUser"
        placeholder="Enter your email"
      />
      <label for="password">Password: </label>
      <input
        type="text"
        formControlName="PasswordUser"
        placeholder="Create a secure password"
      />
      <button type="submit" class="buttonSubmit">Validate</button>
    </form>
  `,
  imports: [ReactiveFormsModule],
  styleUrl: './add-user-form.component.css',
})
export class UserFormComponent {
  addUserForm!: FormGroup;

  userService: UserService = inject(UserService);

  onSubmit() {
    const addUserFormValues = this.addUserForm.value;
    this.userService.addUser(addUserFormValues);
    this.addUserForm.reset();
  }

  ngOnInit(): void {
    this.addUserForm = new FormGroup({
      FirstNameUser: new FormControl('', Validators.required),
      LastNameUser: new FormControl('', Validators.required),
      EmailUser: new FormControl('', Validators.required),
      PasswordUser: new FormControl('', Validators.required),
    });
  }
}
