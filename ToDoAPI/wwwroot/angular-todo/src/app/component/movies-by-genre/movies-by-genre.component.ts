import { Movie } from '../../core/models/movie';
import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovieComponent } from '../movie/movie.component';
import { MovieService } from '../../core/services/movieService';
import { GenresMovies } from '../../core/models/movie';
import { HeaderComponent } from '../../layout/header/header.component';

@Component({
  selector: 'app-movies-by-genre',
  standalone: true,
  imports: [CommonModule, MovieComponent, HeaderComponent],
  template: `
  <app-header></app-header>
`,
  styleUrl: './movies-by-genre.component.css'
})
export class MoviesByGenreComponent {
  movieList: Movie[] = [];
  movie!: Movie;
  genresMoviesList: GenresMovies[] = [];
  genre!: number;

  movieService: MovieService = inject(MovieService);


  constructor() {

    this.movieService
    .getAllMoviesByGenre(this.genre) // Envoie l'ID du genre sélectionné à la fonction
    .then((movieList: Movie[]) => {
      this.movieList = movieList;
      console.log(movieList);
    });
  }


}
