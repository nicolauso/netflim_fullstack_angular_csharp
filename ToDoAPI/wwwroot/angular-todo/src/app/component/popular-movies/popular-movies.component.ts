import { Movie } from '../../core/models/movie';
import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovieComponent } from '../movie/movie.component';
import { MovieService } from '../../core/services/movieService';

@Component({
  selector: 'app-popular-movies',
  standalone: true,
  imports: [CommonModule, MovieComponent],
  template:`
  <section>
  <app-movie *ngFor="let movie of moviePopularList" [movie]="movie"></app-movie>
  </section>
  `,
  styleUrl: './popular-movies.component.css'
})
export class PopularMoviesComponent {
  moviePopularList: Movie[] = [];
  movie!: Movie;
  movieService: MovieService = inject(MovieService);

  constructor() {

    this.movieService
    .getAllPopularMovies()
    .then((moviePopularList: Movie[]) => {
      this.moviePopularList = moviePopularList;
    })
  }
}
