import { Component, Input, EventEmitter, Output, inject } from '@angular/core';
import { Movie } from '../../core/models/movie';
import { CommonModule } from '@angular/common';
import { RouterLink, RouterOutlet } from '@angular/router';
import { MovieService } from '../../core/services/movieService';

@Component({
  selector: 'app-movie-details',
  standalone: true,
  imports: [CommonModule, RouterLink, RouterOutlet],
  template: `
    <section class="listing">
      <img
        class="listing-heading"
        src="https://image.tmdb.org/t/p/w342/{{ movie.poster_path }}"
        alt=""
      />
      <div class="listing-text-content">
        <h2>{{ movie.original_title }}</h2>
        <h4>Release date: {{ movie.release_date }}</h4>
        <p>Synopsis : {{ movie.overview }}</p>
        <h4>Average ratings : {{ movie.vote_average }}</h4>
        <h5>Popularity : {{ movie.popularity }} </h5>
        <ul>
          Genres :
          <li *ngFor="let genre of movie.genre_ids">
            {{ genre }}
          </li>
        </ul>
        <div class="buttons">
          <a [routerLink]="['/details', movie.id]" class="details-link"
            >Learn More</a
          >
          <span class="material-symbols-rounded heart"> favorite </span>
        </div>
      </div>
    </section>
    `,
  styleUrl: './movie-details.component.css',
})
export class MovieDetailsComponent {
  @Input() movie!: Movie;
  movieService: MovieService = inject(MovieService);
}
