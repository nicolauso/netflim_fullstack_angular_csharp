import { Routes } from '@angular/router';
import { HomeComponent } from './layout/home/home.component';
import { PopularMoviesComponent } from './component/popular-movies/popular-movies.component';
import { UserAccountComponent } from './layout/user-account/user-account.component';
import { AdminComponent } from './layout/admin/admin.component';
import { CreatingAccountComponent } from './layout/creating-account/creating-account.component';
import { LoginComponent } from './layout/login/login.component';
import { AuthGuard } from './core/auth-guards';
import { MoviesByGenreComponent } from './component/movies-by-genre/movies-by-genre.component';

export const routes: Routes = [

  {
    path: '',
    component: LoginComponent,
    title: 'Login'
  },

  {
    path: 'home',
    component: HomeComponent,
    title: 'Home',
    canActivate: [AuthGuard]
  },

  {
    path: 'popular',
    component: PopularMoviesComponent,
    title: 'Popular movies',
    canActivate: [AuthGuard]
  },

  {
    path: 'admin',
    component: AdminComponent,
    title: 'Admin',
    canActivate: [AuthGuard]

  },
  {
    path: 'create-account',
    component: CreatingAccountComponent,
    title: 'CreatingAccountComponent',
    canActivate: [AuthGuard]
  },

  {
    path: 'user-account',
    component: UserAccountComponent,
    title: 'UserAccountComponent',
    canActivate: [AuthGuard]
  },
  {
    path: 'moviesByGenre',
    component: MoviesByGenreComponent,
    title: 'MoviesByGenreComponent',
    canActivate: [AuthGuard]
  }

];
