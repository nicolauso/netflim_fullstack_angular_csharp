import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import axios, { HttpStatusCode } from 'axios';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  baseUrlWebApi = 'http://localhost:5129/api/Users';
  constructor(private router: Router) {

  }
  private readonly tokenKey = 'myAppAuthToken';
  async login(loginFormValues: FormGroup) {
    try {
      const response = await axios.post(
        `${this.baseUrlWebApi}/login`,
        loginFormValues
      );
      if (response.status == 200 && response.data) {
        localStorage.setItem(this.tokenKey, response.data)
        this.router.navigate(['home']);
      }
    } catch (error) {
      console.error(error);
    }
  }

  async logout() {
    localStorage.clear();
    this.router.navigate(['']);
    console.log(localStorage.getItem(this.tokenKey));
  }
}
