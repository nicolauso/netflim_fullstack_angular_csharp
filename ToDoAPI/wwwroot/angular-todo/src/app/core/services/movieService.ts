import { Movie } from '../models/movie';
import { GenresMovies } from '../models/movie';
import { Injectable } from '@angular/core';
import axios from 'axios';

@Injectable({
  providedIn: 'root',
})
export class MovieService {
  url = 'http://localhost:5129/api/Movies';
  // url = 'http://localhost:3000/todos';
  key_API = '5a1981fdb85fa8edd3e452fa7886b8ee';
  token =
    'eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI1YTE5ODFmZGI4NWZhOGVkZDNlNDUyZmE3ODg2YjhlZSIsInN1YiI6IjY1Njg5MTQ3ZmI1Mjk5MDEzYzUwZjhmMiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.N1w2TwhtBm_ORZTulLb-O1MhVQKlUls9oqbPGdo-Hrk';

  url_API_allGenres =
    'https://api.themoviedb.org/3/genre/movie/list?language=en';

  url_byGenre =
    'https://api.themoviedb.org/3/discover/movie?language=en-US&with_genres=';

  url_popular_movies =
    'https://api.themoviedb.org/3/movie/popular?language=en-US';

  async getAllGenres() {
    try {
      const response = await axios.get(this.url_API_allGenres, {
        headers: {
          Authorization: `Bearer ${this.token}`,
          Accept: 'application/json',
        },
      });
      console.log('getAllGenres', response.data.genres);

      return response.data.genres;
    } catch (error) {
      console.error('Erreur lors de la requête :', error);
      throw error; // ou return null; ou une autre logique de gestion d'erreur
    }
  }

  async getAllMoviesByGenre(genreId: number) {
    try {
      const response = await axios.get(`${this.url_byGenre}${genreId}`, {
        headers: {
          Authorization: `Bearer ${this.token}`,
          Accept: 'application/json',
        },
      });

      return response.data.results;
    } catch (error) {
      console.error('Erreur lors de la requête :', error);
      throw error; // ou return null; ou une autre logique de gestion d'erreur
    }
  }

  async getAllPopularMovies() {
    try {
      const response = await axios.get(`${this.url_popular_movies}`, {
        headers: {
          Authorization: `Bearer ${this.token}`,
          Accept: 'application/json',
        },
      });
      return response.data.results;
    } catch (error) {
      console.error('Erreur lors de la requête :', error);
      throw error; // ou return null; ou une autre logique de gestion d'erreur
    }
  }

  constructor() {}
}
