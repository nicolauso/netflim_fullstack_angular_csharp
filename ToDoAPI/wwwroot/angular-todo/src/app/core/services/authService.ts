// auth.service.ts
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly tokenKey = 'myAppAuthToken';

  isAuthenticated(): boolean {
    return !!localStorage.getItem(this.tokenKey);
  }
  // login(token: string): void {
  //   localStorage.setItem(this.tokenKey, token);
  // }

  // logout(): void {
  //   localStorage.removeItem(this.tokenKey);
  // }



  // getToken(): string | null {
  //   return localStorage.getItem(this.tokenKey);
  // }
}
