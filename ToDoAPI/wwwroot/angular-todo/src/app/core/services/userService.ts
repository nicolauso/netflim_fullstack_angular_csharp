//  CRUD User Account
// Requête GET, PUT, POST, DELETE à mon back-end pour "déclencher" les manip de données dans ma BDD
import { User } from '../models/user';
import axios from 'axios';
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  // Création nouvel user : Create User = POST: api/Users
  baseUrlWebApi = 'http://localhost:5129/api/Users';

  async addUser(addUserForm: FormGroup) {
    try {
      console.log('addUserForm', addUserForm);
      const response = await axios.post(`${this.baseUrlWebApi}`, addUserForm);
    } catch (error) {
      console.error(error);
    }
  }

  async readUser() {
    try {
      const response = await axios.get(`${this.baseUrlWebApi}`);
      return response.data;
    } catch (error) {
      console.error(error);
    }
  }

  async editUser(userId: number, editUserFormValues: FormGroup) {
    try {
      console.log("User Service récupère les values : editUserFormValues", editUserFormValues);

      const response = await axios.put(`${this.baseUrlWebApi}/${userId}`, editUserFormValues);
    } catch (error) {}
  }

  // { FirstNameUser: "Usertest", LastNameUser: "Usertest", EmailUser: "Usertest@gmail.com", PasswordUser: "efjpezof,e" }

  constructor() {}
}
