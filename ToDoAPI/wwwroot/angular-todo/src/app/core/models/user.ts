import { Movie } from './movie';

export interface User {
userId: number,
firstNameUser: string,
lastNameUser: string,
emailUser: string,
passwordUser: string,
avatar: string,
banner: string,
favorites: Movie[],
watchLater: Movie[],
followings: User[],
myRatings: Rating[]
 isActive: boolean;
}

interface Rating {
  movie: Movie,
  rate: number
}
