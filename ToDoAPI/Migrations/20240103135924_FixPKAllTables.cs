﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ToDoAPI.Migrations
{
    /// <inheritdoc />
    public partial class FixPKAllTables : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Genre",
                columns: table => new
                {
                    GenreId = table.Column<int>(type: "int", nullable: false),
                    NameGenre = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Genre", x => x.GenreId);
                });

            migrationBuilder.CreateTable(
                name: "Movie_Genre",
                columns: table => new
                {
                    Fk_GenreId = table.Column<int>(type: "int", nullable: false),
                    Fk_MovieId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movie_Genre", x => new { x.Fk_GenreId, x.Fk_MovieId });
                });

            migrationBuilder.CreateTable(
                name: "Rating",
                columns: table => new
                {
                    Fk_UserId = table.Column<int>(type: "int", nullable: false),
                    Fk_MovieId = table.Column<int>(type: "int", nullable: false),
                    ValueRating = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rating", x => new { x.Fk_UserId, x.Fk_MovieId });
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "int", nullable: false),
                    LastNameUser = table.Column<string>(type: "varchar(max)", unicode: false, nullable: false),
                    FirstNameUser = table.Column<string>(type: "varchar(max)", unicode: false, nullable: false),
                    EmailUser = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    PasswordUser = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "Movie",
                columns: table => new
                {
                    MovieId = table.Column<int>(type: "int", nullable: false),
                    TitleMovie = table.Column<string>(type: "varchar(max)", unicode: false, nullable: false),
                    OverviewMovie = table.Column<string>(type: "varchar(max)", unicode: false, nullable: true),
                    AdutMovie = table.Column<bool>(type: "bit", nullable: true),
                    BudgetMovie = table.Column<int>(type: "int", nullable: true),
                    FK_GenresMovies = table.Column<int>(type: "int", nullable: true),
                    OriginalLanguageMovie = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
                    OriginalTitleMovie = table.Column<string>(type: "varchar(max)", unicode: false, nullable: true),
                    PosterPathMovie = table.Column<string>(type: "varchar(max)", unicode: false, nullable: true),
                    ReleaseDateMovie = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
                    PopularityMovie = table.Column<int>(type: "int", nullable: true),
                    RuntimeMovie = table.Column<int>(type: "int", nullable: true),
                    RevenueMovie = table.Column<int>(type: "int", nullable: true),
                    StatuMovie = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
                    VideoMovie = table.Column<bool>(type: "bit", nullable: true),
                    RatingAverageMovie = table.Column<double>(type: "float", nullable: true),
                    RatingCountMovie = table.Column<double>(type: "float", nullable: true),
                    TaglineMovie = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movie", x => x.MovieId);
                    table.ForeignKey(
                        name: "FK_Movie_Genre",
                        column: x => x.FK_GenresMovies,
                        principalTable: "Genre",
                        principalColumn: "GenreId");
                });

            migrationBuilder.CreateTable(
                name: "Follower",
                columns: table => new
                {
                    Fk_UserId = table.Column<int>(type: "int", nullable: false),
                    FollowerId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Follower", x => new { x.Fk_UserId, x.FollowerId });
                    table.ForeignKey(
                        name: "FK_Follower_User",
                        column: x => x.Fk_UserId,
                        principalTable: "User",
                        principalColumn: "UserId");
                });

            migrationBuilder.CreateTable(
                name: "Playlist",
                columns: table => new
                {
                    PlaylistId = table.Column<int>(type: "int", nullable: false),
                    TitlePlaylist = table.Column<byte[]>(type: "varbinary(50)", maxLength: 50, nullable: false),
                    FK_UserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Playlist", x => x.PlaylistId);
                    table.ForeignKey(
                        name: "FK_Playlist_User",
                        column: x => x.FK_UserId,
                        principalTable: "User",
                        principalColumn: "UserId");
                });

            migrationBuilder.CreateTable(
                name: "Subscription",
                columns: table => new
                {
                    SubscriptionId = table.Column<int>(type: "int", nullable: false),
                    Fk_UserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subscription", x => new { x.SubscriptionId, x.Fk_UserId });
                    table.ForeignKey(
                        name: "FK_Subscription_User",
                        column: x => x.Fk_UserId,
                        principalTable: "User",
                        principalColumn: "UserId");
                });

            migrationBuilder.CreateTable(
                name: "Comment",
                columns: table => new
                {
                    CommentId = table.Column<int>(type: "int", nullable: false),
                    Fk_MovieId = table.Column<int>(type: "int", nullable: false),
                    Fk_UserId = table.Column<int>(type: "int", nullable: false),
                    DateComment = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
                    ContentComment = table.Column<string>(type: "varchar(max)", unicode: false, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comment", x => x.CommentId);
                    table.ForeignKey(
                        name: "FK_Comment_Movie",
                        column: x => x.Fk_MovieId,
                        principalTable: "Movie",
                        principalColumn: "MovieId");
                    table.ForeignKey(
                        name: "FK_Comment_User",
                        column: x => x.Fk_UserId,
                        principalTable: "User",
                        principalColumn: "UserId");
                });

            migrationBuilder.CreateTable(
                name: "Playlist_Movie",
                columns: table => new
                {
                    PlaylistId = table.Column<int>(type: "int", nullable: false),
                    Fk_MovieId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Playlist_Movie", x => new { x.PlaylistId, x.Fk_MovieId });
                    table.ForeignKey(
                        name: "FK_Playlist_Movie_Movie",
                        column: x => x.Fk_MovieId,
                        principalTable: "Movie",
                        principalColumn: "MovieId");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Comment_Fk_MovieId",
                table: "Comment",
                column: "Fk_MovieId");

            migrationBuilder.CreateIndex(
                name: "IX_Comment_Fk_UserId",
                table: "Comment",
                column: "Fk_UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Movie_FK_GenresMovies",
                table: "Movie",
                column: "FK_GenresMovies");

            migrationBuilder.CreateIndex(
                name: "IX_Playlist_FK_UserId",
                table: "Playlist",
                column: "FK_UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Playlist_Movie_Fk_MovieId",
                table: "Playlist_Movie",
                column: "Fk_MovieId");

            migrationBuilder.CreateIndex(
                name: "IX_Subscription_Fk_UserId",
                table: "Subscription",
                column: "Fk_UserId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Comment");

            migrationBuilder.DropTable(
                name: "Follower");

            migrationBuilder.DropTable(
                name: "Movie_Genre");

            migrationBuilder.DropTable(
                name: "Playlist");

            migrationBuilder.DropTable(
                name: "Playlist_Movie");

            migrationBuilder.DropTable(
                name: "Rating");

            migrationBuilder.DropTable(
                name: "Subscription");

            migrationBuilder.DropTable(
                name: "Movie");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropTable(
                name: "Genre");
        }
    }
}
