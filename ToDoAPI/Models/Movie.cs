﻿using System;
using System.Collections.Generic;

namespace ToDoAPI.Models;

public partial class Movie
{
    public int MovieId { get; set; }

    public string TitleMovie { get; set; } = null!;

    public string? OverviewMovie { get; set; }

    public bool? AdutMovie { get; set; }

    public int? BudgetMovie { get; set; }

    public int? FkGenresMovies { get; set; }

    public string? OriginalLanguageMovie { get; set; }

    public string? OriginalTitleMovie { get; set; }

    public string? PosterPathMovie { get; set; }

    public string? ReleaseDateMovie { get; set; }

    public int? PopularityMovie { get; set; }

    public int? RuntimeMovie { get; set; }

    public int? RevenueMovie { get; set; }

    public string? StatuMovie { get; set; }

    public bool? VideoMovie { get; set; }

    public double? RatingAverageMovie { get; set; }

    public double? RatingCountMovie { get; set; }

    public string? TaglineMovie { get; set; }

    public virtual ICollection<Comment> Comments { get; set; } = new List<Comment>();

    public virtual Genre? FkGenresMoviesNavigation { get; set; }

    public virtual ICollection<PlaylistMovie> PlaylistMovies { get; set; } = new List<PlaylistMovie>();
}
