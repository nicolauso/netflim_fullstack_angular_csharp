﻿using System;
using System.Collections.Generic;

namespace ToDoAPI.Models;

public partial class Playlist
{
    public int PlaylistId { get; set; }

    public byte[] TitlePlaylist { get; set; } = null!;

    public int FkUserId { get; set; }

    public virtual User FkUser { get; set; } = null!;
}
