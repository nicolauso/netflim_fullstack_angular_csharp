﻿using System;
using System.Collections.Generic;

namespace ToDoAPI.Models;

public partial class User
{
    public int UserId { get; set; }

    public string LastNameUser { get; set; } = null!;

    public string FirstNameUser { get; set; } = null!;

    public string EmailUser { get; set; } = null!;

    public string PasswordUser { get; set; } = null!;

    public virtual ICollection<Comment> Comments { get; set; } = new List<Comment>();

    public virtual ICollection<Follower> Followers { get; set; } = new List<Follower>();

    public virtual ICollection<Playlist> Playlists { get; set; } = new List<Playlist>();

    public virtual ICollection<Subscription> Subscriptions { get; set; } = new List<Subscription>();
}
