﻿using System;
using System.Collections.Generic;

namespace ToDoAPI.Models;

public partial class MovieGenre
{
    public int FkGenreId { get; set; }

    public int FkMovieId { get; set; }
}
