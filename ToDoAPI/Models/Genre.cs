﻿using System;
using System.Collections.Generic;

namespace ToDoAPI.Models;

public partial class Genre
{
    public int GenreId { get; set; }

    public string NameGenre { get; set; } = null!;

    public virtual ICollection<Movie> Movies { get; set; } = new List<Movie>();
}
