﻿using System;
using System.Collections.Generic;

namespace ToDoAPI.Models;

public partial class Rating
{
    public int FkUserId { get; set; }

    public int FkMovieId { get; set; }

    public int? ValueRating { get; set; }
}
