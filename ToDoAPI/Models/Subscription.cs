﻿using System;
using System.Collections.Generic;

namespace ToDoAPI.Models;

public partial class Subscription
{
    public int SubscriptionId { get; set; }

    public int FkUserId { get; set; }

    public virtual User FkUser { get; set; } = null!;
}
