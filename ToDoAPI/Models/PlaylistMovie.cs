﻿using System;
using System.Collections.Generic;

namespace ToDoAPI.Models;

public partial class PlaylistMovie
{
    public int PlaylistId { get; set; }

    public int FkMovieId { get; set; }

    public virtual Movie FkMovie { get; set; } = null!;
}
