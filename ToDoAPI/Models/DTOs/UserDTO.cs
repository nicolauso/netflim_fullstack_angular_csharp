﻿namespace ToDoAPI.Models.DTO
{
    public class UserDTO
    {

        public class UserDto
        {
            public int UserId { get; set; }
            public string LastNameUser { get; set; } = null!;
            public string FirstNameUser { get; set; } = null!;
            public string EmailUser { get; set; } = null!;
            public string UserPasswordHashed { get; set; } = null!;
        }

        public class UserEditDto
        {
            public int UserId { get; set; }
            public string LastNameUser { get; set; } = null!;
            public string FirstNameUser { get; set; } = null!;
            public string EmailUser { get; set; } = null!;
        }


        public class UserLoginDTO
        {
          
            public string EmailUser { get; set; } = null!;
            public string PasswordUser { get; set; } = null!;
        }
    }
}
