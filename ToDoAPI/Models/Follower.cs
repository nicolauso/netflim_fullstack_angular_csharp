﻿using System;
using System.Collections.Generic;

namespace ToDoAPI.Models;

public partial class Follower
{
    public int FkUserId { get; set; }

    public int FollowerId { get; set; }

    public virtual User FkUser { get; set; } = null!;
}
