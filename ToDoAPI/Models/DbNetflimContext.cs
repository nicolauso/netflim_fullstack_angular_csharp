﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace ToDoAPI.Models;

public partial class DbNetflimContext : DbContext
{

    public DbNetflimContext(DbContextOptions<DbNetflimContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Comment> Comments { get; set; }

    public virtual DbSet<Follower> Followers { get; set; }

    public virtual DbSet<Genre> Genres { get; set; }

    public virtual DbSet<Movie> Movies { get; set; }

    public virtual DbSet<MovieGenre> MovieGenres { get; set; }

    public virtual DbSet<Playlist> Playlists { get; set; }

    public virtual DbSet<PlaylistMovie> PlaylistMovies { get; set; }

    public virtual DbSet<Rating> Ratings { get; set; }

    public virtual DbSet<Subscription> Subscriptions { get; set; }

    public virtual DbSet<User> Users { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        => optionsBuilder.UseSqlServer("Server=(localdb)\\local;Database=DB_Netflim;Trusted_Connection=True;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Comment>(entity =>
        {
            entity.ToTable("Comment");

            entity.Property(e => e.CommentId).ValueGeneratedOnAdd();
            entity.Property(e => e.ContentComment).IsUnicode(false);
            entity.Property(e => e.DateComment)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.FkMovieId).HasColumnName("Fk_MovieId");
            entity.Property(e => e.FkUserId).HasColumnName("Fk_UserId");

            entity.HasOne(d => d.FkMovie).WithMany(p => p.Comments)
                .HasForeignKey(d => d.FkMovieId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Comment_Movie");

            entity.HasOne(d => d.FkUser).WithMany(p => p.Comments)
                .HasForeignKey(d => d.FkUserId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Comment_User");
        });

        modelBuilder.Entity<Follower>(entity =>
        {
            entity.HasKey(e => new { e.FkUserId, e.FollowerId });

            entity.ToTable("Follower");

            entity.Property(e => e.FkUserId).HasColumnName("Fk_UserId");

            entity.HasOne(d => d.FkUser).WithMany(p => p.Followers)
                .HasForeignKey(d => d.FkUserId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Follower_User");
        });

        modelBuilder.Entity<Genre>(entity =>
        {
            entity.ToTable("Genre");

            entity.Property(e => e.GenreId).ValueGeneratedOnAdd();
            entity.Property(e => e.NameGenre)
                .HasMaxLength(50)
                .IsUnicode(false);
        });

        modelBuilder.Entity<Movie>(entity =>
        {
            entity.ToTable("Movie");

            entity.Property(e => e.MovieId).ValueGeneratedOnAdd();
            entity.Property(e => e.FkGenresMovies).HasColumnName("FK_GenresMovies");
            entity.Property(e => e.OriginalLanguageMovie)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.OriginalTitleMovie).IsUnicode(false);
            entity.Property(e => e.OverviewMovie).IsUnicode(false);
            entity.Property(e => e.PosterPathMovie).IsUnicode(false);
            entity.Property(e => e.ReleaseDateMovie)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.StatuMovie)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.TaglineMovie)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.TitleMovie).IsUnicode(false);

            entity.HasOne(d => d.FkGenresMoviesNavigation).WithMany(p => p.Movies)
                .HasForeignKey(d => d.FkGenresMovies)
                .HasConstraintName("FK_Movie_Genre");
        });

        modelBuilder.Entity<MovieGenre>(entity =>
        {
            entity.HasKey(e => new { e.FkGenreId, e.FkMovieId });

            entity.ToTable("Movie_Genre");

            entity.Property(e => e.FkGenreId).HasColumnName("Fk_GenreId");
            entity.Property(e => e.FkMovieId).HasColumnName("Fk_MovieId");
        });

        modelBuilder.Entity<Playlist>(entity =>
        {
            entity.ToTable("Playlist");

            entity.Property(e => e.PlaylistId).ValueGeneratedOnAdd();
            entity.Property(e => e.FkUserId).HasColumnName("FK_UserId");
            entity.Property(e => e.TitlePlaylist).HasMaxLength(50);

            entity.HasOne(d => d.FkUser).WithMany(p => p.Playlists)
                .HasForeignKey(d => d.FkUserId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Playlist_User");
        });

        modelBuilder.Entity<PlaylistMovie>(entity =>
        {
            entity.HasKey(e => new { e.PlaylistId, e.FkMovieId });

            entity.ToTable("Playlist_Movie");

            entity.Property(e => e.FkMovieId).HasColumnName("Fk_MovieId");

            entity.HasOne(d => d.FkMovie).WithMany(p => p.PlaylistMovies)
                .HasForeignKey(d => d.FkMovieId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Playlist_Movie_Movie");
        });

        modelBuilder.Entity<Rating>(entity =>
        {
            entity.HasKey(e => new { e.FkUserId, e.FkMovieId });

            entity.ToTable("Rating");

            entity.Property(e => e.FkUserId).HasColumnName("Fk_UserId");
            entity.Property(e => e.FkMovieId).HasColumnName("Fk_MovieId");
        });

        modelBuilder.Entity<Subscription>(entity =>
        {
            entity.HasKey(e => new { e.SubscriptionId, e.FkUserId });

            entity.ToTable("Subscription");

            entity.Property(e => e.FkUserId).HasColumnName("Fk_UserId");

            entity.HasOne(d => d.FkUser).WithMany(p => p.Subscriptions)
                .HasForeignKey(d => d.FkUserId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Subscription_User");
        });

        modelBuilder.Entity<User>(entity =>
        {
            entity.ToTable("User");

            entity.Property(e => e.UserId).ValueGeneratedOnAdd();
            entity.Property(e => e.EmailUser)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.FirstNameUser).IsUnicode(false);
            entity.Property(e => e.LastNameUser).IsUnicode(false);
            entity.Property(e => e.PasswordUser)
                .HasMaxLength(50)
                .IsUnicode(false);
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
