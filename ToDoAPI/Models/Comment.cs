﻿using System;
using System.Collections.Generic;

namespace ToDoAPI.Models;

public partial class Comment
{
    public int CommentId { get; set; }

    public int FkMovieId { get; set; }

    public int FkUserId { get; set; }

    public string? DateComment { get; set; }

    public string? ContentComment { get; set; }

    public virtual Movie FkMovie { get; set; } = null!;

    public virtual User FkUser { get; set; } = null!;
}
