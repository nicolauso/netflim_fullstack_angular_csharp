﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ToDoAPI.Models;

namespace ToDoAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlaylistMoviesController : ControllerBase
    {
        private readonly DbNetflimContext _context;

        public PlaylistMoviesController(DbNetflimContext context)
        {
            _context = context;
        }

        // GET: api/PlaylistMovies
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PlaylistMovie>>> GetPlaylistMovies()
        {
            return await _context.PlaylistMovies.ToListAsync();
        }

        // GET: api/PlaylistMovies/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PlaylistMovie>> GetPlaylistMovie(int id)
        {
            var playlistMovie = await _context.PlaylistMovies.FindAsync(id);

            if (playlistMovie == null)
            {
                return NotFound();
            }

            return playlistMovie;
        }

        // PUT: api/PlaylistMovies/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPlaylistMovie(int id, PlaylistMovie playlistMovie)
        {
            if (id != playlistMovie.PlaylistId)
            {
                return BadRequest();
            }

            _context.Entry(playlistMovie).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PlaylistMovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/PlaylistMovies
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<PlaylistMovie>> PostPlaylistMovie(PlaylistMovie playlistMovie)
        {
            _context.PlaylistMovies.Add(playlistMovie);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (PlaylistMovieExists(playlistMovie.PlaylistId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetPlaylistMovie", new { id = playlistMovie.PlaylistId }, playlistMovie);
        }

        // DELETE: api/PlaylistMovies/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePlaylistMovie(int id)
        {
            var playlistMovie = await _context.PlaylistMovies.FindAsync(id);
            if (playlistMovie == null)
            {
                return NotFound();
            }

            _context.PlaylistMovies.Remove(playlistMovie);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool PlaylistMovieExists(int id)
        {
            return _context.PlaylistMovies.Any(e => e.PlaylistId == id);
        }
    }
}
