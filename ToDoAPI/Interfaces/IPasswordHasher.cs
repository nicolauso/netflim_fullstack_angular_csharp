﻿using Microsoft.AspNetCore.Identity;

namespace ToDoAPI.Interfaces
{
    public interface IPasswordHasher<TUser> where TUser : class
    {
        string HashedPassword(TUser user, string password);

        PasswordVerificationResult VerifyHashedPassword(
            TUser user, string hashedPassword, string providedPassword);
    }

}
